<?php

namespace dir\core;

use dir\core\Route;
use dir\core\DB;
use dir\core\Model;

/**
 * Class of base controller
 */
class Controller {

    /**
     * Contains object of class View
     * 
     * @var Object
     */
    public static $view;

    /**
     * Contains object of class Model
     * 
     * @var Object
     */
    public static $model;

    /**
     * Contains object of class DB
     * 
     * @var Object
     */
    public static $db;

    /**
     * Contains array with values from database
     * 
     * @var Array
     */
    public static $datas;

    /**
     * Create of class View and save him in self::$view
     *
     * @return void
     */
    public function __construct() {
        self::$view = new View();
    }

    /**
     * Create of class Model and save him in self::$model
     *
     * @return void
     */
    public function runModel() {
        $modelPath = '\dir\application\models\\' . Route::getController() . '\\' . Route::getAction();
        self::$model = new $modelPath();
    }

    /**
     * Saves values in db from Model::$archive
     *
     * @return void
     */
    public function saveInDB() {
        self::$db = DB::getConnection();
        try {
            $query = "INSERT INTO `archive` (`id`, `data`) VALUES (NULL, :data)";
            $add = self::$db->prepare($query);
            $add->bindValue(':data', json_encode(Model::$archive, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK));
            $add->execute();
        } catch (PDOException $e) {
            echo 'Some errors';
        }
    }

    /**
     * Gets values from database and saves them in self::$datas
     *
     * @return void
     */
    public function getFromDB() {
        self::$db = DB::getConnection();
        try {
            $query = self::$db->query("SELECT data FROM `archive` ORDER BY id DESC");
            foreach ($query as $string) {
                self::$datas[] = $string['data'];
            }
        } catch (PDOException $e) {
            echo 'Can\'t select from the database';
        }
    }

}
