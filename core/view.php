<?php

namespace dir\core;

/**
 * Class of base view
 */
class View {

    /**
     * Creates view and exit
     *
     * @return void
     */
    public function render($view) {
        require_once 'application/views/layout.php';
        require_once 'application/views/' . Route::getController() . '/' . $view . '.php';
        require_once 'application/views/footer.php';
        exit();
    }

}
