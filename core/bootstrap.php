<?php

use \dir\core\Route;
use \dir\core\Controller;

//get names of controller and action on URL
$route = Route::getInstance();
$route->setProperties();

//create controller
$controllerPath = '\dir\application\controllers\\' . Route::getController();
$controller = new $controllerPath();

//start action
$Action = Route::getAction();
$controller->$Action();

//create view
Controller::$view->render(Route::getAction());
