<?php

namespace dir\core;

use PDO;
use PDOException;

/**
 * Class for connecting to database
 */
class DB {

    /**
     * Contains object of PDO
     *
     * @var Object
     */
    private static $pdo;

    /**
     * Contains single object of this class
     *
     * @var Object
     */
    private static $instance;

    /**
     * Connecting to database
     *
     * @return void
     */
    private function __construct() {
        try {
            self::$pdo = new PDO('mysql:host=localhost;dbname=parser', user, 1234);
            self::$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            self::$pdo->exec('SET NAMES "utf8"');
        } catch (PDOException $e) {
            exit('Error connecting to database!' . $e);
        }
    }

    /**
     * Creates an object or return an existing one (singleton)
     *
     * @return void
     */
    public static function getConnection() {
        if (empty(self::$instance)) {
            self::$instance = new DB();
        }
        return self::$pdo;
    }

}
