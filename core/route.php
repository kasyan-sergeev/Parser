<?php

namespace dir\core;

/**
 * Class of routing
 */
class Route {

    /**
     * Contains name of controller
     *
     * @var String
     */
    private static $controller;

    /**
     * Contains name of action
     *
     * @var String
     */
    private static $action;

    /**
     * Contains single object of this class
     *
     * @var Object
     */
    private static $instance;

    private function __construct() {
        
    }

    /**
     * Creates an object or return an existing one (singleton)
     *
     * @return Object
     */
    public static function getInstance() {
        if (empty(self::$instance)) {
            self::$instance = new Route();
        }
        return self::$instance;
    }

    /**
     * Sets values for controller and action
     *
     * @return void
     */
    public function setProperties() {
        $url = explode('/', rtrim($_GET['route'], '/'));
        $pathController = '\dir\application\controllers\\' . $url[0];

        if (empty($url[0])) {
            self::$controller = 'Parsing';
        } elseif (class_exists($pathController) == false) {
            self::$controller = 'Error';
        } else {
            self::$controller = $url[0];
        }

        if (!empty($url[1])) {
            if (method_exists($pathController, $url[1])) {
                self::$action = $url[1];
            } else {
                self::$controller = 'Error';
                self::$action = 'Index';
            }
        } else {
            self::$action = 'Index';
        }
    }

    public static function getController() {
        return self::$controller;
    }

    public static function getAction() {
        return self::$action;
    }

}
