<?php

use dir\application\models\parsing\Links;

if (isset(Links::$archive)) {
    $archive = Links::$archive;
}
?>
<h2>Parsing links</h2>
<form method="POST">
    <div class="form-group">
        <label>Write URL</label>
        <input type="text" class="form-control" name="URL"  value="<?= $archive['url']; ?>">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
<div>
    <?php
    if (isset(Links::$archive)) {
        echo '<b>Count: ' . $archive[count] . '</b><br><b>List:</b><br>';
        foreach ($archive[URLlinks] as $value) {
            echo $value . '<br>';
        }
    }
    ?>
</div>