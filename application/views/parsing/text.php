<?php

use dir\application\models\parsing\Text;

if (isset(Text::$archive)) {
    $archive = Text::$archive;
}
?>
<h2>Parsing text</h2>
<form method="POST">
    <div class="form-group">
        <label>Write URL</label>
        <input type="text" class="form-control" name="URL" value="<?= $archive['url']; ?>">
    </div>
    <div class="form-group">
        <label>Write text</label>
        <input type="text" class="form-control" name="text" value="<?= $archive['text']; ?>">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
<div>
    <?php
    if (isset(Text::$archive)) {
        echo '<b>Count: ' . $archive[count] . '</b><br>';
    }
    ?>
</div>