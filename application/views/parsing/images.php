<?php

use dir\application\models\parsing\Images;

if (isset(Images::$archive)) {
    $archive = Images::$archive;
}
?>
<h2>Parsing images</h2>
<form method="POST">
    <div class="form-group">
        <label>Write URL</label>
        <input type="text" class="form-control" name="URL" value="<?= $archive['url']; ?>">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
<div>
    <?php
    if (isset(Images::$archive)) {
        echo '<b>Count: ' . $archive[count] . '</b><br><b>List:</b><br>';
        foreach ($archive[imageLinks] as $value) {
            echo $value . '<br>';
        }
    }
    ?>
</div>