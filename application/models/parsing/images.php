<?php

namespace dir\application\models\parsing;

use dir\core\Model;

/**
 * Class of model for parsing images
 */
class Images extends Model {

    /**
     * Saves values in Model::$archive
     *
     * @return void
     */
    public function __construct() {
        $host = parse_url($_POST['URL'], PHP_URL_HOST);
        require 'libs/phpQuery-onefile.php';
        $url = $_POST['URL'];
        $file = file_get_contents($url);
        $doc = \phpQuery::newDocument($file);
        $images = $doc->find("img");
        $count = 0;
        foreach ($images as $image) {
            $count++;
            if (strpos($image->getAttribute('src'), ':') === false) {
                $image->setAttribute('src', 'http://' . $host . $image->getAttribute('src'));
            }
            $imageLinks[] = $image->getAttribute('src');
        }
        parent::$archive = compact(count, imageLinks, url);
    }

}
