<?php

namespace dir\application\models\parsing;

use dir\core\Model;

/**
 * Class of model for parsing text
 */
class Text extends Model {

    /**
     * Saves values in Model::$archive
     *
     * @return void
     */
    public function __construct() {
        $host = parse_url($_POST['URL'], PHP_URL_HOST);
        require 'libs/phpQuery-onefile.php';
        $url = $_POST['URL'];

        $text = $_POST['text'];
        $file = file_get_contents($url);
        $file = iconv("windows-1251", "UTF-8", $file);

        $count = substr_count($file, $text);

        parent::$archive = compact(count, text, url);
    }

}
