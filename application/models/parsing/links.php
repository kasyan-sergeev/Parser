<?php

namespace dir\application\models\parsing;

use dir\core\Model;

/**
 * Class of model for parsing links
 */
class Links extends Model {

    /**
     * Saves values in Model::$archive
     *
     * @return void
     */
    public function __construct() {
        $host = parse_url($_POST['URL'], PHP_URL_HOST);
        require 'libs/phpQuery-onefile.php';
        $url = $_POST['URL'];
        $file = file_get_contents($url);
        $doc = \phpQuery::newDocument($file);
        $links = $doc->find("a");
        $count = 0;
        foreach ($links as $link) {
            $count++;
            if (strpos($link->getAttribute('href'), ':') === false) {
                $link->setAttribute('href', 'http://' . $host . $link->getAttribute('href'));
            }
            $URLlinks[] = $link->getAttribute('href');
        }
        parent::$archive = compact(count, URLlinks, url);
    }

}
