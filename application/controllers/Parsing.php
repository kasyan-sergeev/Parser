<?php

namespace dir\application\controllers;

use dir\core\Controller;
use dir\core\Model;

/**
 * Class of parsing controller
 */
class Parsing extends Controller {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Method for index action, control is passed to view
     *
     * @return void
     */
    public function Index() {
        
    }

    public function images() {
        if (!empty($_POST['URL'])) {
            parent::runModel();
            if (!empty(Model::$archive)) {
                parent::saveInDB();
            }
        }
    }

    public function links() {
        if (!empty($_POST['URL'])) {
            parent::runModel();
            if (!empty(Model::$archive)) {
                parent::saveInDB();
            }
        }
    }

    public function text() {
        if (!empty($_POST['URL'])) {
            parent::runModel();
            if (!empty(Model::$archive)) {
                parent::saveInDB();
            }
        }
    }

}
