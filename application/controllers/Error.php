<?php

namespace dir\application\controllers;

use dir\core\Controller;

/**
 * Class of error controller
 */
class Error extends Controller {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Empty method for index action, control is passed to view
     *
     * @return void
     */
    public function Index() {
        
    }

}
