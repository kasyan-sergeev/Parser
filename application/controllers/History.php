<?php

namespace dir\application\controllers;

use dir\core\Controller;

/**
 * Class of history controller
 */
class History extends Controller {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Method for index action
     *
     * @return void
     */
    public function Index() {
        parent::getFromDB();
    }

}
